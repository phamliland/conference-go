from attendees.models import Attendee
from .models import AccountVO, ConferenceVO
from common.json import ModelEncoder


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "name",
        "import_href"
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name"
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        account_count = AccountVO.objects.filter(email=o.email).count()
        # print("account_count", account_count)
        if account_count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}
