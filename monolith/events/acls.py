import requests
from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }

    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = response.json()
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Use the Open Weather API
    url = "https://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    data = response.json()
    try:
        latitude = data[0]["lat"]
        longitude = data[0]["lon"]
    except (KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    content = response.json()

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


get_weather_data("Houston", "TX")
