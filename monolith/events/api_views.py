import json
from django.http import JsonResponse
from events.acls import get_weather_data, get_photo
from events.encoder import (
    ConferenceDetailEncoder,
    ConferenceListEncoder,
    LocationDetailEncoder,
    LocationListEncoder,
)
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder
        )
    else:
        content = json.loads(request.body)
    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "invalid location id"},
            status=400
        )
    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        # checking the request method from browser
        conference = Conference.objects.get(id=id)

        # get_weather_data function from acls.py (line 47)
        weather = get_weather_data(conference.location.city, conference.location.state.abbreviation)
        # fetching a conference instance based on an id, assigning a variable
        return JsonResponse(
            {"conference": conference, "weather": weather},
            encoder=ConferenceDetailEncoder,
            # don't necessarily need this line on 51 since it's returning a dictionary on line 50
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            # need safe=False if not retuning a dictionary
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)
    try:
        state = State.objects.get(abbreviation=content["state"])
        content["state"] = state

    except State.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid state abbreviation"},
            status=400,
        )
    # calling get_photo function in acls.py (line 99-100)
    photo = get_photo(content["city"], content["state"].abbreviation)
    content.update(photo)

    location = Location.objects.create(**content)
    return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
